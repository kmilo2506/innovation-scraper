# innovation-scraper

Python web scraper for Endava Innovation Labs

This project runs a webcrawler named ‘meetups’ to scrap the webpage [Find your people | Meetup](https://www.meetup.com/find/events/?allMeetups=true&radius=25&userFreeform=Bogot%C3%A1%2C+Colombia&mcId=c1006695&mcName=Bogot%C3%A1%2C+CO). For upcoming events and their associated tags.

To run the crawler follow this instructions:

1. Install pipenv `pip install pipenv`
2. CD into the project root and run `pipenv install`. This will download all dependencies for the project and create a virtual environment for it.
3. Run `pipenv shell` to activate the virtualenv
4. Run `scrapy crawl meetups -o meetups.json`. This will crawl the Bogota events page from the meetups website and output the results to a file `meetups.json`
