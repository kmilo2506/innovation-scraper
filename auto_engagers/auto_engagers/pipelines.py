# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from psycopg2 import connect


class MeetupsPipeline(object):

    def __init__(self, db_host, db_name, db_username, db_password):
        self.db_host = db_host
        self.db_name = db_name
        self.db_username = db_username
        self.db_password = db_password

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            db_host=crawler.settings.get('DB_HOSTNAME'),
            db_name=crawler.settings.get('DB_NAME'),
            db_username=crawler.settings.get('DB_USERNAME'),
            db_password=crawler.settings.get('DB_PASSWORD')
        )

    def open_spider(self, spider):
        self.conn = connect(host=self.db_host, user=self.db_username, password=self.db_password, dbname=self.db_name)
        self.cursor = self.conn.cursor()

    def close_spider(self, spider):
        self.cursor.close()
        self.conn.close()

    def process_item(self, item, spider):
        if not item.get('tags', None) is None:
            self.cursor.execute("""INSERT INTO events(name, tags) VALUES (%s, %s) """,
                                (item['name'], item['tags']))
            self.conn.commit()
        return item
