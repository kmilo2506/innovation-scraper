import scrapy
from scrapy import Selector
from scrapy.loader import ItemLoader
from ..items import EventItem

TAGS = 'tags'

HREF = 'href'

NAME = 'name'

ORGANIZER = 'organizer'


class Event:

    def __init__(self, organizer, name, href, tags=None):
        if tags is None:
            tags = []
        self.organizer = organizer
        self.name = name
        self.href = href
        self.tags = tags


class MeetupsSpider(scrapy.Spider):
    name = 'meetups'

    start_urls = [
        "https://www.meetup.com/find/events/?allMeetups=true&radius=25&userFreeform=Bogot%C3%A1%2C+Colombia&mcId=c1006695&mcName=Bogot%C3%A1%2C+CO",
    ]

    def parse(self, response):
        event_selector = Selector(text=response.body, type='html')
        events = event_selector.xpath('//li[re:test(@class, "row.event-listing")]')
        parsed_events = []

        loader = ItemLoader(item=EventItem(), response=response)
        for event in events:
            loader.add_value(ORGANIZER, event.css('span::text')[1].extract())
            loader.add_value(NAME, event.css('span::text')[2].extract())
            loader.add_value(HREF, event.css('a::attr(href)')[1].extract())
            loader.load_item()

        organizers = loader.get_collected_values(ORGANIZER)
        names = loader.get_collected_values(NAME)
        hrefs = loader.get_collected_values(HREF)

        for i in range(0, len(organizers)):
            parsed_events.append(Event(organizers[i], names[i], hrefs[i]))

        for e in parsed_events:
            request = scrapy.Request(e.href, callback=self.parse_tags)
            request.meta['event'] = e
            yield request

    def parse_tags(self, response):
        tags_selector = Selector(text=response.body, type='html')
        tags = tags_selector.xpath('//div[re:test(@class, "topicsList.flush--left.margin--top")]').css(
            '::text').extract()
        loader = ItemLoader(item=EventItem(), response=response)
        event_wo_tags = response.meta['event']
        tags.append(event_wo_tags.organizer)

        loader.add_value(ORGANIZER, event_wo_tags.organizer)
        loader.add_value(NAME, event_wo_tags.name)
        loader.add_value(HREF, event_wo_tags.href)
        loader.add_value(TAGS, tags)

        loader.load_item()

        yield loader.item
